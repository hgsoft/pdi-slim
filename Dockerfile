FROM alpine
LABEL "maintainer"="Alexsandro Haag" 
LABEL "email"="alex@hgsoft.com.br"

# Set env variaveis
ENV PENTAHO_HOME=/home/pentaho 

ENV PATH=${PATH}:${PENTAHO_HOME}/data-integration:${PENTAHO_HOME}/bin

RUN addgroup --system pentaho && \
    adduser -s /bin/bash -h ${PENTAHO_HOME} -S -G pentaho pentaho

RUN apk --no-cache add curl tar fontconfig ttf-dejavu tzdata openjdk11-jre

RUN cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    echo "America/Sao_Paulo" >  /etc/timezone

USER pentaho

WORKDIR $PENTAHO_HOME

#Para download do Pentaho ao invés de usar local
RUN curl -L -o /tmp/pdi-slim.tar.bz2 \
	https://gitlab.com/hgsoft/pdi-slim/-/archive/main/pdi-slim-main.tar.bz2 && \
	/usr/bin/tar -jxf /tmp/pdi-slim.tar.bz2 -C $PENTAHO_HOME --strip-components=1 && \
	rm /tmp/pdi-slim.tar.bz2

# CMD echo "Build Completo"
CMD kitchen.sh etc/index.kjb dev Minimal
