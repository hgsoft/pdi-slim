# pdi-slim

Fork do Pentaho Data Integration enxugando a estrutura para que rode mais rapidamente e fique menor para gerar imagens docker.
 - PDI versão 9.3
 - Java jre 11

## Como usar o Container

### Fazer o build da imagem
```
docker build --tag pdi_slim:9.3 .
```

### Como executar um job
```
docker run -it pdi_slim:9.3 kitchen.sh
```

### Como executa uma transformação
```
docker run -it pdi_slim:9.3 pan.sh
```

### Tamanho da Imagem
```
REPOSITORY                                    TAG                    IMAGE ID       CREATED          SIZE
pdi_slim                                      9.3                    e68860e7103c   43 minutes ago   503MB
```

### Tempo de Execução
```
 time docker run -it pdi_slim:9.3 kitchen.sh
Illegal option -p
#######################################################################
WARNING:  no libwebkitgtk-1.0 detected, some features will be unavailable
    Consider installing the package with apt-get or yum.
    e.g. 'sudo apt-get install libwebkitgtk-1.0-0'
#######################################################################
Options:
  -rep            = Repository name
  -user           = Repository username
  -trustuser      = !Kitchen.ComdLine.RepUsername!
  -pass           = Repository password
  -job            = The name of the job to launch
  -dir            = The directory (dont forget the leading /)
  -file           = The filename (Job XML) to launch
  -level          = The logging level (Basic, Detailed, Debug, Rowlevel, Error, Minimal, Nothing)
  -logfile        = The logging file to write to
  -listdir        = List the directories in the repository
  -listjobs       = List the jobs in the specified directory
  -listrep        = List the available repositories
  -norep          = Do not log into the repository
  -version        = show the version, revision and build date
  -param          = Set a named parameter <NAME>=<VALUE>. For example -param:FILE=customers.csv
  -listparam      = List information concerning the defined parameters in the specified job.
  -export         = Exports all linked resources of the specified job. The argument is the name of a ZIP file.
  -custom         = Set a custom plugin specific option as a String value in the job using <NAME>=<Value>, for example: -custom:COLOR=Red
  -maxloglines    = The maximum number of log lines that are kept internally by Kettle. Set to 0 to keep all rows (default)
  -maxlogtimeout  = The maximum age (in minutes) of a log line while being kept internally by Kettle. Set to 0 to keep all rows indefinitely (default)


real    0m2,679s
user    0m0,050s
sys     0m0,059s

```

## Drivers JDBC Inclusos
 - Oracle 
 - Mysql 
 - MariaDB
 - MSSQL
 - Firebird

## Referências
 - Artigo das sugestões de limpeza do PDI - https://blog.twineworks.com/improving-startup-time-of-pentaho-data-integration-78d0803c559b
